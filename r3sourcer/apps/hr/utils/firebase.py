import os
from datetime import datetime, timedelta
import firebase_admin
from firebase_admin import credentials, messaging, firestore
from django.conf import settings


basepath = os.path.dirname(__file__)
filepath = os.path.abspath(os.path.join(
    basepath, "..", "..", "..", "..", "keys", "serviceAccountKey.json")
)

credential = credentials.Certificate({
  "type": settings.TYPE,
  "project_id": settings.PROJECT_ID,
  "private_key": settings.PRIVATE_KEY.replace(r'\n', '\n'),
  "client_email": settings.CLIENT_EMAIL,
  "token_uri": settings.TOKEN_URI,
})

firebase_admin.initialize_app(credential)
db = firestore.client()


def send_job_offer_notification(fcm_token):
    message = messaging.Message(
        notification=messaging.Notification(
            title="You've received job offer",
            body="There is job offer for you. Please accept or decline it.",
        ),
        data=None,
        token=fcm_token
    )
    response = messaging.send(message)
    print('Successfully message sent:', response)


def send_job_offer_notifications(fcm_tokens):
    message = messaging.MulticastMessage(
        notification=messaging.Notification(
            title="You've received job offer",
            body="There is job offer for you. Please accept or decline it.",
        ),
        data=None,
        tokens=fcm_tokens
    )
    response = messaging.send_each_for_multicast(message)
    print('Successfully messages sent:', response)


def get_multiple_fcm_tokens(email):
    docs = db.collection("users").document(email).collection('tokens').where(
        "createdAt", ">=", datetime.utcnow() - timedelta(hours=24)
    ).get()
    tokens = [doc.to_dict()['token'] for doc in docs]
    return tokens


def get_single_fcm_token(email, doc):
    doc = db.collection("users").document(email).collection('tokens').document(doc).get()
    token = doc.to_dict()['token']
    return token


def delete_stale_tokens(email):
    # docs = (db.collection("users").document(email).collection('tokens').stream())
    # for doc in docs:
    #     mydict = doc.to_dict()
    #     token = mydict['token']
    #     if token == TOKEN_VAL:
    #         doc.reference.delete()

    docs = db.collection("users").document(email).collection('tokens').where(
        "createdAt", "<", datetime.utcnow() - timedelta(hours=24)
    ).get()

    for doc in docs:
        doc.reference.delete()
